Assume you have an Apollo monolith graph server as in here.

It can execute queries to retrieve movies and mutations to create movies.

Migrate to Apollo Federation Graph
Define subgraph
Define supergraph
Add multiple Sub Graph to the Federation Graph
Resolve complex fields using multiple subgraphs
Migrate to Apollo Federation Graph
Define subgraph
The subgraph needs the same schema-type definitions and needs to add unique identifiers to define it as an entity. Subgraph shape foundation types can be defined as entities.

const typeDefs = gql`
    # movie type defines the queryable fields of Movie
    type Movie @key(fields: "id"){
        id: ID!
        name: String
        duration: Int
        genre: String
        views: Int
    }
    input MovieInput {
        id: ID!
        name: String
        duration: Int
        genre: String
        views: Int
    }
    # Query type of the movies graph returns the movies graph shape
    type Query {
        movie(id: ID!): Movie
        movies: [Movie]
    }
    type Mutation {
        createMovie(movie: MovieInput): Movie!
    }
`;
It can use the same resolver for the movie subgraph.

const fetch = require("node-fetch");
const apiUrl = "http://localhost:3000";

const resolvers = {
    Query: {
        movie(_, { id }) {
            return fetch(`${apiUrl}/movies/${id}`).then(res => res.json());
        },
        movies() {
            return fetch(`${apiUrl}/movies`).then(res => res.json());
        }
    },
    Mutation: {
        createMovie(root, args, ctx) {
            const { movie } = args;
            const headers = {
                "Content-Type": "application/json"
            }
            fetch(`${apiUrl}/movies`, {
                method: "post",
                body: JSON.stringify(movie),
                headers
            })
            .then(res => res.json())
            .then(json => console.log(json))
            .catch(err => console.log(err))
            return movie;
        }
    }
};
The subgraph server should be defined with the schema of the subgraph.

const { ApolloServer, gql } = require("apollo-server");
const { buildSubgraphSchema } = require("@apollo/subgraph");

const port = 4001;

const server = new ApolloServer({
    schema: buildSubgraphSchema([{ typeDefs, resolvers }])
});

server.listen({ port }).then(({ url }) => {
    console.log(`Movies Graph has successfully started and listening at ${url}`);
});
The subgraph definition should be as follows.

const { ApolloServer, gql } = require("apollo-server");
const { buildSubgraphSchema } = require("@apollo/subgraph");
const fetch = require("node-fetch");

const port = 4001;
const apiUrl = "http://localhost:3000";

/**
 * A schema is a collection of type definitions (typeDefs).
 * All the defined types together define the "shape" of queries that are executed against the backend/data.
 */
const typeDefs = gql`
    # movie type defines the queryable fields of Movie
    type Movie @key(fields: "id"){
        id: ID!
        name: String
        duration: Int
        genre: String
        views: Int
    }
    input MovieInput {
        id: ID!
        name: String
        duration: Int
        genre: String
        views: Int
    }
    # Query type of the movies graph returns the movies graph shape
    type Query {
        movie(id: ID!): Movie
        movies: [Movie]
    }
    type Mutation {
        createMovie(movie: MovieInput): Movie!
    }
`;

/**
 * Resolvers define how to fetch the types defined in the schema
 */
const resolvers = {
    Query: {
        movie(_, { id }) {
            return fetch(`${apiUrl}/movies/${id}`).then(res => res.json());
        },
        movies() {
            return fetch(`${apiUrl}/movies`).then(res => res.json());
        }
    },
    Mutation: {
        createMovie(root, args, ctx) {
            const { movie } = args;
            const headers = {
                "Content-Type": "application/json"
            }
            fetch(`${apiUrl}/movies`, {
                method: "post",
                body: JSON.stringify(movie),
                headers
            })
            .then(res => res.json())
            .then(json => console.log(json))
            .catch(err => console.log(err))
            return movie;
        }
    }
};

/**
 * For subgraph it needs to define as the schema of the subgraph
 * For the single Apollo graphql server need to pass as follow
        const server = new ApolloServer({
                typeDefs,
                resolvers
            });
 */
const server = new ApolloServer({
    schema: buildSubgraphSchema([{ typeDefs, resolvers }])
});

server.listen({ port }).then(({ url }) => {
    console.log(`Movies Graph has successfully started and listening at ${url}`);
});
Define Supergraph
The supergraph should contain the gateway of the subgraphs.

const { ApolloGateway } = require("@apollo/gateway");

const port = 4000;

/**
 * Gateway needs the subgraph service list as an argument
 */
const gateway = new ApolloGateway({
    serviceList: [{
        name: "movies", url: "http://localhost:4001"
    }]
});
Supergraph also needs a server along with details of the gateway and subgraphs.

const { ApolloServer } = require("apollo-server");

const server = new ApolloServer({
    gateway,
    subscriptions: false
});

server.listen({ port }).then(({ url }) => {
    console.log(`Graph gateway has successfully started and listening at ${url}`);
});
Update package.json with dependencies and update scripts to start the movie subgraph.

{
  "name": "graphql-apollo-federation-demo",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "dependencies": {
    "concurrently": "^5.1.0",
    "json-server": "^0.16.1",
    "apollo-server": "^3.12.0",
    "graphql": "^16.7.1",
    "node-fetch": "^2.6.0",
    "nodemon": "^2.0.3",
    "@apollo/gateway": "^2.4.5",
    "@apollo/subgraph": "^2.4.5"
  },
  "scripts": {
    "server": "concurrently -k npm:server:*",
    "server:rest": "json-server -q db.json",
    "server:movies": "nodemon movies.js",
    "server:graphql": "nodemon index.js"
  },
  "author": "Pawara Gunawardena",
  "license": "ISC"
}
Previous queries and mutations should work after migrating to the federation.

curl --location 'http://localhost:4000/' \
--header 'Content-Type: application/json' \
--data '{"query":"    query Movies {\n        movies {\n            id\n            name\n            genre\n            duration\n            views\n        }\n    }","variables":{}}'
curl --location 'http://localhost:4000/' \
--header 'Content-Type: application/json' \
--data '{"query":"    query Movie($id: ID!) {\n        movie (id: $id) {\n            id\n            name\n            genre\n            duration\n            views\n        }\n    }","variables":{"id":2}}'
curl --location 'http://localhost:4000/' \
--header 'Content-Type: application/json' \
--data '{"query":"mutation Mutation($movie: MovieInput) {\n  createMovie(movie: $movie) {\n    id\n    name\n    duration\n    genre\n    views\n  }\n}","variables":{"movie":{"id":"6","name":"Ride","genre":"Adventure","duration":105,"views":750}}}'