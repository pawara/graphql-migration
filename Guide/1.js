---

GraphQL Apollo Federation
Let's get started on how to migrate from a GraphQL monolith to Apollo Federation with a super graph and multiple sub-graphs.
Sections describe the below stages.
Setup the backend server
Setup GraphQL monolith server and query
Mutations on GraphQL monolith server

---

Setup the backend server and investigate data
JSON server is used as the backend server. Follow the below steps to initiate the project.
Create an empty folder for the project

2. Follow npm init or copy the below package.json into the project folder.
{
  "name": "graphql-apollo-federation-demo",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",  
  "dependencies": {
    "json-server": "^0.16.1"
  },
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "Pawara Gunawardena",
  "license": "ISC"
}
3. Run the below command to install dependencies
npm install
4. Copy the following JSON file to use as the mock data of the backend server.
{
  "movies": [
    {
      "id": 1,
      "name": "Fly a kite",
      "duration": 85,
      "genre": "Fantasy",
      "views": 1200
    },
    {
      "id": 2,
      "name": "Summer Bay",
      "duration": 110,
      "genre": "Adventure",
      "views": 380
    },
    {
      "id": 3,
      "name": "Tide Ride",
      "duration": 85,
      "genre": "Drama",
      "views": 215
    },
    {
      "id": 4,
      "name": "Capnella",
      "duration": 95,
      "genre": "Drama",
      "views": 650
    },
    {
      "id": 5,
      "name": "Chemicals",
      "duration": 88,
      "genre": "Science Fiction",
      "views": 1020
    }
  ],
  "prices": [
    {
      "id": 1,
      "referenceEntityId": 1,
      "entityPrice": {
        "amount": 7,
        "currency": "USD"
      },
      "serviceCharges": {
        "stream" : {
          "amount": 1,
          "currency": "USD"
        },
        "support" : {
          "amount": 0.1,
          "currency": "USD"
        }
      }
    },
    {
      "id": 2,
      "referenceEntityId": 2,
      "entityPrice": {
        "amount": 5,
        "currency": "USD"
      },
      "serviceCharges": {
        "stream" : {
          "amount": 0.9,
          "currency": "USD"
        },
        "support" : {
          "amount": 0.15,
          "currency": "USD"
        }
      }
    },
    {
      "id": 3,
      "referenceEntityId": 3,
      "entityPrice": {
        "amount": 5,
        "currency": "USD"
      },
      "serviceCharges": {
        "stream" : {
          "amount": 0.6,
          "currency": "USD"
        },
        "support" : {
          "amount": 0.15,
          "currency": "USD"
        }
      }
    },
    {
      "id": 4,
      "referenceEntityId": 4,
      "entityPrice": {
        "amount": 6,
        "currency": "USD"
      },
      "serviceCharges": {
        "stream" : {
          "amount": 1.2,
          "currency": "USD"
        },
        "support" : {
          "amount": 0.2,
          "currency": "USD"
        }
      }
    },
    {
      "id": 5,
      "referenceEntityId": 5,
      "entityPrice": {
        "amount": 4,
        "currency": "USD"
      },
      "serviceCharges": {
        "stream" : {
          "amount": 0.7,
          "currency": "USD"
        },
        "support" : {
          "amount": 0.1,
          "currency": "USD"
        }
      }
    }
  ],
  "discounts": [
    {
      "id": 1,
      "referenceEntityId": 1,
      "validityPeriod": {
        "beginMonth": 8,
        "endMonth":11
      },
      "amount": 1,
      "type": "amount"
    },
    {
      "id": 2,
      "referenceEntityId": 5,
      "validityPeriod": {
        "beginMonth": 7,
        "endMonth":10
      },
      "amount": 20,
      "type": "percentage"
    }
  ]
}
5. Start the JSON server
json-server -q db.json
6. Query the backend mock server and validate that the server is accessible.
curl --location 'http://localhost:3000/movies'
curl --location 'http://localhost:3000/movies/2'
curl --location 'http://localhost:3000/movies/' \
--header 'Content-Type: application/json' \
--data '{
    "id": 6,
    "name": "Shelter",
    "duration": 90,
    "genre": "Fantasy",
    "views": 300
}'
7. Project structure should look like this <GitHub URL>

---

Setup GraphQL monolith server and query movies from the backend
8. Update the package.json and execute the npm install
Update dependencies with the below packages.
"concurrently": "^5.1.0",
"apollo-server": "^3.12.0",
"graphql": "^16.7.1",
"node-fetch": "^2.6.0",
"nodemon": "^2.0.3"
Update scripts to start the backend and GraphQL server.

"scripts": {
  "server": "concurrently -k npm:server:*",
  "server:rest": "json-server -q db.json",
  "server:graphql": "nodemon index.js"
}
9. Add index.js file to the project
10. Let's define the schema of the movies graph.
Define the Query type of the movies graph to return a list of movies.
type Query {
    movies: [Movie]
}
When GraphQL tries to resolve movies, it will look into the Movie object type which also needs to be defined as an object type.
type Movie {
    id: ID!
    name: String
    duration: Int
    genre: String
    views: Int
}
11. Define the resolver of the movies field.
As we investigated the backend server data, the Movie type contains all scalar fields of  the <backend base url>/movies endpoint response. But how to map the <backend base url>/movies response to the GraphQL schema? To accomplish that, it needs to define the resolver for the movies.
const fetch = require("node-fetch");

const apiUrl = "http://localhost:3000";

const resolvers = {
    Query: {
        movies() {
            return fetch(`${apiUrl}/movies`).then(res => res.json());
        }
    }
};
12. Implement the Movies graph.
Now it has the main building blocks of the Movies graph, tpe definitions schema and resolvers. Let's implement the Apollo GraphQL server.
const { ApolloServer, gql } = require("apollo-server");

const port = 4000;

const server = new ApolloServer({
    typeDefs,
    resolvers
});

server.listen({ port }).then(({ url }) => {
    console.log(`Movies Graph has successfully started and listening at ${url}`);
});
After adding these changes index.js file looks like below.
const { ApolloServer, gql } = require("apollo-server");
const fetch = require("node-fetch");

const port = 4000;
const apiUrl = "http://localhost:3000";

const typeDefs = gql`
    type Movie {
        id: ID!
        name: String
        duration: Int
        genre: String
        views: Int
    }

    type Query {
        movies: [Movie]
    }
`;

const resolvers = {
    Query: {
        movies() {
            return fetch(`${apiUrl}/movies`).then(res => res.json());
        }
    }
};

const server = new ApolloServer({
    typeDefs,
    resolvers
});

server.listen({ port }).then(({ url }) => {
    console.log(`Movies Graph has successfully started and listening at ${url}`);
});
13. Start the backend and Movies graph.
npm run server
14. Execute GraphQL queries on the graph of the movies
curl --location 'http://localhost:4000/' \
--header 'Content-Type: application/json' \
--data '{"query":"    query Movies {\n        movies {\n            id\n            name\n            genre\n            duration\n            views\n        }\n    }","variables":{}}'
Project content after implementation of movies resolver link
15. Update to resolve a single movie by ID
Update schema and resolver.
const typeDefs = gql`
    type Movie {
        id: ID!
        name: String
        duration: Int
        genre: String
        views: Int
    }

    type Query {
        movie(id: ID!): Movie
        movies: [Movie]
    }
`;

const resolvers = {
    Query: {
        movie(_, { id }) {
            return fetch(`${apiUrl}/movies/${id}`).then(res => res.json());
        },
        movies() {
            return fetch(`${apiUrl}/movies`).then(res => res.json());
        }
    }
};
Project content after implementation of a movie resolver by id link
16. Execute GraphQL query to resolve a movie by ID.
curl --location 'http://localhost:4000/' \
--header 'Content-Type: application/json' \
--data '{"query":"    query Movie($id: ID!) {\n        movie (id: $id) {\n            id\n            name\n            genre\n            duration\n            views\n        }\n    }","variables":{"id":2}}'

---

Mutations on GraphQL monolith server

17. In addition to the type Query, add type Mutation to the schema along with createMovie key value
type Mutation {
    createMovie(movie: MovieInput): Movie!
}
According to this definition, define the schema of MovieInput object as an input.
input MovieInput {
    id: ID!
    name: String
    duration: Int
    genre: String
    views: Int
}
18. Define the Mutation resolver for the createMovie
Mutation: {
    createMovie(root, args, ctx) {
        const { movie } = args;
        const headers = {
            "Content-Type": "application/json"
        }
        fetch(`${apiUrl}/movies`, {
            method: "post",
            body: JSON.stringify(movie),
            headers
        })
        .then(res => res.json())
        .then(json => console.log(json))
        .catch(err => console.log(err))
        return movie;
    }
}
Project content after implementation of create movie mutation schema and resolver link
19. Execute GraphQL mutation to create a movie.
curl --location 'http://localhost:4000/' \
--header 'Content-Type: application/json' \
--data '{"query":"mutation Mutation($movie: MovieInput) {\n  createMovie(movie: $movie) {\n    id\n    name\n    duration\n    genre\n    views\n  }\n}","variables":{"movie":{"id":"8","name":"Order of the Phoenix 2","genre":"Adventure","duration":105,"views":750}}}'
20. In addition to the curl commands, it is possible to access Explorer to execute queries and mutations on GraphQL.
In a web browser, visit the GraphQL server URL and it will navigate to the Explorer.