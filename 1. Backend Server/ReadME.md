

1. Install dependencies
        npm install

2. Start the mock JSON server

        json-server -q db.json

3. Invetigate data

        curl --location 'http://localhost:3000/movies'

        curl --location 'http://localhost:3000/movies/2'
